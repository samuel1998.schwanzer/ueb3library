package com.samuel.microservice.ueb3library.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.samuel.microservice.ueb3library.model.rest.BookPostRest;
import com.samuel.microservice.ueb3library.model.rest.BookRest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@ToString
@AllArgsConstructor
@Getter
@Setter
public class BookPostRestImpl implements BookPostRest {
	@JsonProperty("title")
	@NotBlank
	private String title;
	@NotBlank
	@JsonProperty("author")
	private String author;

}
