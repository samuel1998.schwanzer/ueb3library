package com.samuel.microservice.ueb3library.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import com.samuel.microservice.ueb3library.model.rest.ReviewRest;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReviewRestImpl implements ReviewRest {
	@JsonProperty("id")
	@Min(0)
	private int id;
	@NotBlank
	@JsonProperty("text")
	private String text;
	@NotBlank
	@JsonProperty("score")
	private String score;
	@JsonProperty("bookId")
	@Min(0)
	private int bookId;
}
