package com.samuel.microservice.ueb3library.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.samuel.microservice.ueb3library.model.dao.Book;
import lombok.*;
import com.samuel.microservice.ueb3library.model.rest.BookRest;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookRestImpl implements BookRest {
	@JsonProperty("id")
	@Min(0)
	private int id;
	@NotBlank
	@JsonProperty("author")
	private String author;
	@NotBlank
	@JsonProperty("title")
	private String title;
}
