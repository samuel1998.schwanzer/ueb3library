package com.samuel.microservice.ueb3library.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.samuel.microservice.ueb3library.model.rest.ReviewPostRest;
import com.samuel.microservice.ueb3library.model.rest.ReviewRest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@Getter
@Setter
public class ReviewPostRestImpl implements ReviewPostRest {
	@JsonProperty("text")
	@NotBlank
	private String text;
	@NotBlank
	@JsonProperty("score")
	private String score;
	@JsonProperty("bookId")
	@Min(0)
	private int bookId;
}
