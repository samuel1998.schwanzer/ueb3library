package com.samuel.microservice.ueb3library.exceptions;

import javax.validation.ConstraintViolation;
import java.util.Set;

public class GlobalException extends RuntimeException{
	private final String exceptionMessage;
	public static final String header= "Ein Fehler ist aufgetreten";

	public GlobalException(final Set<? extends ConstraintViolation<?>> setOfViolations){
		final StringBuilder messageBuilder = new StringBuilder();
		System.out.println(setOfViolations);
		for (ConstraintViolation<?> constraintViolation: setOfViolations){;
			messageBuilder.append(constraintViolation.getPropertyPath().toString().substring(0, 1).toUpperCase())
			              .append(constraintViolation.getPropertyPath().toString().substring(1))
						  .append(" ")
			              .append(constraintViolation.getMessage())
			              .append("\n");
		}

		exceptionMessage = messageBuilder.toString();
	}

	public String getMessage(){
		return exceptionMessage;
	}
}
