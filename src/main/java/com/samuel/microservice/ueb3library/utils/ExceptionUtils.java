package com.samuel.microservice.ueb3library.utils;


import com.samuel.microservice.ueb3library.dao.model.BookDaoImpl;
import com.samuel.microservice.ueb3library.exceptions.GlobalException;
import com.samuel.microservice.ueb3library.model.dao.Book;
import com.samuel.microservice.ueb3library.model.dao.Review;
import lombok.experimental.UtilityClass;
import org.apache.tomcat.jni.Address;

import javax.validation.ConstraintDeclarationException;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

@UtilityClass
public class ExceptionUtils {
	private static Validator validator = null;


	static{
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}

	public static String getNoItemSelected(){
		return "Kein Element ausgewählt";
	}


	public static String getNoItemSelectedDetailed(){
		return "Für diese Funktion muss ein Element aus der Liste ausgewählt werden";
	}


	public static void checkException(final Set<? extends ConstraintViolation<?>> setOfViolations){
		if (setOfViolations.size()>0){
			throw new GlobalException(setOfViolations);
		}
	}

	public static void validateBook(final Book book){
		checkException(validator.validate(book));
	}

	public static void validateReview(final Review review){
		checkException(validator.validate(review));
	}
}
