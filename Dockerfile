FROM maven:3.8.3-openjdk-17 as BUILD

COPY src /usr/src/myapp/src
COPY pom.xml /usr/src/myapp
RUN mvn -f /usr/src/myapp/pom.xml clean package

FROM openjdk:18-jdk-alpine as RUNNING

COPY --from=BUILD /usr/src/myapp/target/ueb3library-0.0.1-SNAPSHOT.jar /library.jar
ENTRYPOINT ["java","-jar","/library.jar"]
